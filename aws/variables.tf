variable "instance_name" {
  type = string
}

variable "machine_type" {
  type = string
}

variable "machine_zone" {
  type = string
}

variable "boot_disk_image" {
  type = string
}

variable "boot_disk_type" {
  type = string
}