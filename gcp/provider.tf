provider "google" {
  # credentials = file("../smart-pride-353020-28d52ffc0671.json")
  credentials = file("serviceaccount.json")

  project = "smart-pride-353020"
  region  = "us-central1"
  zone    = "us-central1-b"
}
